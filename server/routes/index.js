var express = require('express');
var router = express.Router();
const schema = require('../schema/schema');
const { graphqlHTTP } = require('express-graphql');

router.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));

module.exports = router;
