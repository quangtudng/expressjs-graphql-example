const graphql = require('graphql');
const { find, filter } = require('lodash');
const {
  GraphQLObjectType,
  GraphQLString, 
  GraphQLSchema,
  GraphQLID, 
  GraphQLInt,
  GraphQLList
} = graphql;

const mockBook = [
  { id: 1, name: 'Fate Grand Order', genre: 'Fantasy', authorId: 1 },
  { id: 2, name: 'World War II', genre: 'History', authorId: 3 },
  { id: 3, name: 'Fighting with me', genre: 'History', authorId: 2 },
  { id: 4, name: 'Kill la kill', genre: 'Comic book', authorId: 1 },
  { id: 5, name: 'Fate Stay Night', genre: 'Fantasy', authorId: 3 },
  { id: 6, name: 'Stay awesome bro', genre: 'History', authorId: 2 },
]
const mockAuthor = [
  { id: 1, name: 'Johnny Sin', age: '69' },
  { id: 2, name: 'Lord Gabe', age: '35' },
  { id: 3, name: 'Even You', age: '42' },
]

const Author = new GraphQLObjectType({
  name: 'Author',
  // Field must be wrapped in a function to ignore the order of different types
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    books: {
      type: new GraphQLList(Book),
      resolve(parent, args) {
        return filter(mockBook, { authorId: parent.id })
      }
    }
  })
});

const Book = new GraphQLObjectType({
  name: 'Book',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    genre: { type: GraphQLString },
    // Get author of a book
    author: {
      type: Author,
      resolve(parent, args) {
        return find(mockAuthor, { id: Number.parseInt(parent.authorId) });
      }
    }
  })
});

const rootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    // Get book by id
    book: {
      type: Book,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return find(mockBook, { id: Number.parseInt(args.id) });
      }
    },
    author: {
      type: Author,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return find(mockAuthor, { id: Number.parseInt(args.id) });
      }
    }
  }
})
module.exports = new GraphQLSchema({
  query: rootQuery,
})
// Schema uses: 
// 1) Define type
// 2) Define relationship
// 3) Define root query
// rootQuery: Entry point of user query